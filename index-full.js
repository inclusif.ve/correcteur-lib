var rules = require('./db-build/règles.json')
var dataRaw = require('./db-build/guide.json')


var api = {}

//cb = function(err, word)
api.inclusify = function(word, cb)
{
  if (rules[word])
  {
    return cb(null, rules[word])
  }
  else
  {
    if (dataRaw.includes(word))
    {

      var dataLine = dataRaw.split('\n')
      var res = ""

      for (var i = 0; i < dataLine.length; i++)
      {
        if (dataLine[i].includes(word))
        {

          res += "\n---------------------------\n"
          var start = Math.max(0, i-5)
          var end = Math.min(dataLine.length, i+5+1)
          for (var j = start; j < end; j++)
          {
            if (j === i)
            {
              res += dataLine[j] + '\t <=== \n'
            }
            else
            {
               res += dataLine[j] + '\n'
            }
          }

          res += "\n---------------------------\n"
        }
      }


        return cb('Erreur: ' + word + ' n\'existe pas dans la base de données mais il a été trouvé dans le guide original. Peut être qu\'un oeil humain peut le déchiffrer ! Si c\'est le cas, merci de le signaler sur le repo git !\n\n' + res, word)
    }
    return cb('Erreur: ' + word + ' n\'existe pas dans la base de données', word)
  }
}


module.exports = api

function testInclusify()
{
  var words = ['videur', 'veloutier', 'spectographiste', 'rizicultrice', 'étalonneur', 'azertyuiop', 'speaker', 'stylicien']
  for (var i = 0; i < words.length; ++i)
  {
    api.inclusify(words[i], function(err, word)
    {
      if (err)
      {
        console.log(err)
      }
      console.log(words[i] + " => " + word)
    })
  }

}
