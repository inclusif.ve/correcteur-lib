#!/bin/bash

# ce script créé une bibliothèque qui fonctionne dans un navigateur
browserify index.js | minify --js > correcteur.min.js

browserify index-full.js | minify --js > correcteur-full.min.js
