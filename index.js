var rules = require('./db-build/règles.json')


var api = {}

//cb = function(err, word)
api.inclusify = function(word, cb)
{
  if (rules[word])
  {
    return cb(null, rules[word])
  }
  else
  {
    return cb('Erreur: ' + word + ' n\'existe pas dans la base de données', word)
  }
}


module.exports = api

function testInclusify()
{
  var words = ['videur', 'veloutier', 'spectographiste', 'rizicultrice', 'étalonneur']
  for (var i = 0; i < words.length; ++i)
  {
    api.inclusify(words[i], function(err, word)
    {
      console.log(words[i] + " => " + word)
    })
  }

}
